// encrypt-file.js
const openpgp = require("openpgp");
const fs = require("fs");

const publicKeyArmored = `-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEYYFxgBYJKwYBBAHaRw8BAQdAis/ihp23URxGE4ZSntUqu74Nm5aFFaRc
eSnBj5N1o/PNHHBlcnNvbiA8cGVyc29uQHNvbWVib2R5LmNvbT7CjAQQFgoA
HQUCYYFxgAQLCQcIAxUICgQWAAIBAhkBAhsDAh4BACEJEMvKdICeIwVSFiEE
VqcB5hv8y3IUE4h5y8p0gJ4jBVJrdgD+PH2YUPKufUEl67rvwZB5dyJkDgSe
FcYO6WmGyjXAX9AA/Rk9z662ivHB3mNq2QdfnnYAvOc/uNfK0yWQsAHLMPAA
zjgEYYFxgBIKKwYBBAGXVQEFAQEHQED9ZW/H2EhuKxDZgawnFL1O+Zsu10CC
fxPvje2/DMFhAwEIB8J4BBgWCAAJBQJhgXGAAhsMACEJEMvKdICeIwVSFiEE
VqcB5hv8y3IUE4h5y8p0gJ4jBVI6HwEA8gmQnonRdwftfiiBX+WkZopDlYJ4
MaA5n224zBM+4QcBAKNMotZCaxVzLOA+LxmGyH2Epi+N07FOb9+YFDjg2QIJ
=kqaf
-----END PGP PUBLIC KEY BLOCK-----`

encrypt();
async function encrypt() {
  const plainData = fs.readFileSync("secrets.txt");
  const encrypted = await openpgp.encrypt({
    message: await openpgp.createMessage({text: plainData.toString()}),
    encryptionKeys: (await openpgp.readKey({armoredKey: publicKeyArmored}))
  });

  fs.writeFileSync("encrypted-secrets.txt", encrypted);
}