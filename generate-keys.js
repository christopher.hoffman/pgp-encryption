// generate-keys.js
const openpgp = require("openpgp");

generate();
async function generate() {
  const { privateKey, publicKey } = await openpgp.generateKey({
    userIDs: [{ name: "person", email: "person@somebody.com" }],
    curve: "ed25519",
    passphrase: "qwerty",
    type: 'ecc'
  });
  console.log(privateKey);
  console.log(publicKey);
}