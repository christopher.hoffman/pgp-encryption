# Test of the openpgp library.

Based on this tutorial: https://www.loginradius.com/blog/async/using-pgp-encryption-with-nodejs/
(though updated to use openpgp v3.0+)

Generate keys using `node generate-keys.js`, then save each key to `public.txt` and `private.txt`.

Encrypt the `secrets.txt` by running `node encrypt-file.js`, which will create a `encrypted-secrets.txt`.

Decrypt the `encrypted-secrets.txt` by running `node decrypt-file.js`, which will console.log the decrypted contents.