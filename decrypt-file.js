// decrypt-file.js
const openpgp = require("openpgp");
const fs = require("fs");

const privateKeyArmored = `-----BEGIN PGP PRIVATE KEY BLOCK-----

xYYEYYFxgBYJKwYBBAHaRw8BAQdAis/ihp23URxGE4ZSntUqu74Nm5aFFaRc
eSnBj5N1o/P+CQMIRv4iWGpHe6PggkwOu56aF3dTJALsPT2AqmVgyt8T/d9u
oY+k5k9GKfc/pys0G0gfNhu2JdIXAT6jh4tA2cWjF4tKcQHazKSqt/VzKiZh
OM0ccGVyc29uIDxwZXJzb25Ac29tZWJvZHkuY29tPsKMBBAWCgAdBQJhgXGA
BAsJBwgDFQgKBBYAAgECGQECGwMCHgEAIQkQy8p0gJ4jBVIWIQRWpwHmG/zL
chQTiHnLynSAniMFUmt2AP48fZhQ8q59QSXruu/BkHl3ImQOBJ4Vxg7paYbK
NcBf0AD9GT3PrraK8cHeY2rZB1+edgC85z+418rTJZCwAcsw8ADHiwRhgXGA
EgorBgEEAZdVAQUBAQdAQP1lb8fYSG4rENmBrCcUvU75my7XQIJ/E++N7b8M
wWEDAQgH/gkDCP/EcZTPqSoL4PIp6wOfj/BN2z5eVYzTjcE7XwHTpIq5IC/3
yruLXv7k5UalMNTtGFjS06Ho6d1X1dLqstTeidjK7aNA1ggkvklh7LvqSJrC
eAQYFggACQUCYYFxgAIbDAAhCRDLynSAniMFUhYhBFanAeYb/MtyFBOIecvK
dICeIwVSOh8BAPIJkJ6J0XcH7X4ogV/lpGaKQ5WCeDGgOZ9tuMwTPuEHAQCj
TKLWQmsVcyzgPi8Zhsh9hKYvjdOxTm/fmBQ44NkCCQ==
=Sp5X
-----END PGP PRIVATE KEY BLOCK-----`
const passphrase = `qwerty`;

decrypt();
async function decrypt() {
  const privateKey = await openpgp.decryptKey({
    privateKey: await openpgp.readPrivateKey({ armoredKey: privateKeyArmored }),
    passphrase
});

  const encryptedData = fs.readFileSync("encrypted-secrets.txt");

  const message = await openpgp.readMessage({
    armoredMessage: encryptedData.toString() // parse armored message
  });
  const { data: decrypted, signatures } = await openpgp.decrypt({
      message,
      decryptionKeys: privateKey
  });

  console.log(decrypted);
}